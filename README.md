# aub-repo
Okay so this is the main repository. The programs are organized by letters that represent
different kinds of programs in them. In actuality, gats doesn't care if these programs
are organized like this. It's more for the readability of the repository for humans.
	Below is a list of all the categories in the repository. In each category there are
individual folders for each program. Those folders then (usually) contain three files
within them. The package.rm file is a small readme provided by the repo maintenance team
which explains a bit about the package in plain english. The package.nfo file contains
gats readable information about the package, basically a package.rm for the program to
be able to understand. Finally, the package.sh file is what's actually responsible for
installing or removing the file, whereas gats install.sh is more of a tool to search for
the program and organize its dependencies. 

# dir name and whats inside it list
A	Aubergine specific software (mostly just gats and ACE) and official Aubergine dotfiles
B	Base linux system
C	unsorted Command line tools
D	programming and Development tools (GCC is in here)
F	typefacing tools and Fonts
G	Games
I	system Information tools
K	the linux Kernel
L	all the programs that belong to "Lennart userland" ie systemd, pulseaudio, wayland
N	Network and sysadmin tools
Q	Qt graphical API libraries and tools
V	Version control software (git and subversion)
W	Web browsers, torrent clients, WWW stuff basically
X	Xorg and the insane amount of shit associated with it
Xe	desktop Environments and window managers (-gnome and GTK which is in L)
|	since all DEs and WMs to a lesser extent are clusterfucks they have their own dirs
|-KDE currently the only DE that aubergine ships with
Y	various unsorted programs for X (idk dude figure it out)
Z	archive and file management tools

if you wanna know anything else you should ask the repo maintanence team
whats that?
im the only one working on this distro?
i AM the repo maintenance team?
then ask me tomorrow ',:^ j
