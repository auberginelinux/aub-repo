#This is the management script for neofetch
#source gats.conf, declare cat and name, source nfo from gats, cat, and name, dec other
source /etc/gats/gats.conf
CAT="I"
NAME="neofetch"
source $REPODIR/$CAT/$NAME/$NAME.nfo
MAKE="make $MAKEOPTS"
CDIR="/usr/src/$NAME-$VERSION"
echo ===============================================
echo Switching to neofetch.sh installation script...
echo ===============================================
if [ "$1" = "install" ]; then
	echo Recieved command to install $NAME!
	if [ -d $CDIR ]; then
		echo $CDIR Already exists! If you want to reinstall, please run:
		echo "'rm -rf $CDIR' as root and try again."
		exit
	fi
	mkdir -v $CDIR
	echo Cloning into $CDIR...
	echo
	git clone $SRCL $CDIR
	cd $CDIR
	echo
	echo Changed directory to $CDIR!
	echo Running $MAKE install...
	$MAKE install
	echo Installation of $NAME complete!
elif [ "$1" = "uninstall" ]; then
	echo Recieved command to remove $NAME!
	echo Gathering information from $CDIR...
	cd $CDIR
	echo Changed directory to $CDIR!
	echo Running $MAKE uninstall...
	$MAKE uninstall
	echo $NAME has been removed.
elif [ "$1" != "install" ] && [ "$1" != "uninstall" ]; then
 echo CALLED WRONGLY!
 echo BAD CALL OR NO CALL!
 echo EXITING WITH FATAL ERROR!
 exit
fi
